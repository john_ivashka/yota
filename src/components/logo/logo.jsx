import React, { Component } from 'react';
import './logo.css';

export default class Logo extends Component {
    startShot = () => {
      document.getElementById('nuf').classList.add('logo--short');
      setTimeout(() => document.getElementById('nuf').classList.remove('logo--short'), 2000);
    }
    render() {
        return (
            <div onClick={this.startShot} className='logo' id='nuf' />
        );
    }
}
