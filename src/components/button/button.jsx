import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './button.css';

export default class Button extends Component {
    static propTypes = {
        text: PropTypes.string,
        onClick: PropTypes.func
    };

    static defaultProps = {
        text: 'Кнопка',
    };

    render() {
        return (
            <button onClick={this.props.onClick} className='button'>
                {this.props.text}
            </button>
        );
    }
}
