import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Icon from '../icon'

import './folder-icon.css';

export default class FolderIcon extends Component {
    render() {
        return (
            <Icon className='folder-icon' nameIcon={this.props.isOpen ? 'folder_open' : 'folder'} />  
        );
    }
}
