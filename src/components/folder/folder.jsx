import React, { Component } from 'react';
import PropTypes from 'prop-types';

import FolderIcon from '../folder-icon';
import Icon from '../icon';

import './folder.css';

export default class Folder extends Component {
    state = {
        isOpenFolder: false
    };

    toggleFolder = () => {
        this.setState(state => ({ isOpenFolder: !this.state.isOpenFolder }));
    }

    render() {
        return (
            <div className='folder' tabIndex='0' onClick={this.toggleFolder}>
              <FolderIcon isOpen={this.state.isOpenFolder} />
              <span className='folder__name'>Папка номер 1</span>
              <div className='folder__edit'>
                <Icon nameIcon='edit' color='inherit' />
                <Icon nameIcon='delete' color='inherit' />
              </div>
            </div>
        );
    }
}
