import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Icon from '../icon'

import './close.css';

export default class Close extends Component {
    static propTypes = {
        size: PropTypes.string
    };

    static defaultProps = {
        size: 'l',
    };

    render() {
        return (
            <button
                tabIndex='0'
                onClick={this.props.onClosePopup}
                className={`close`}
            >
                <Icon color='white' nameIcon='clear' />
            </button>
        );
    }
}
