import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './icon.css';

export default class Icon extends Component {
    static propTypes = {
        nameIcon: PropTypes.string,
        color: PropTypes.string
    };

    static defaultProps = {
        color: 'blue',
    };

    render() {
        return (
            <i className={`material-icons icon--${this.props.color}`}>{this.props.nameIcon}</i>
        );
    }
}
