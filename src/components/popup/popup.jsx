import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Close from '../close/close';
import Button from '../button/button';

import './popup.css';

export default class Popup extends Component {
    addOpenAnimation = () => {
        document.getElementById('popup').classList.add('popup--open-animation');
    }

    componentWillMount() {
        this.modal = document.createElement('div');
        document.body.appendChild(this.modal);
    }

    componentDidMount() {
        setTimeout(this.addOpenAnimation, 2);
    }

    componentWillUnmount() {
        document.body.removeChild(this.modal);
    }

    render() {
        return ReactDOM.createPortal(
            <form className='popup' id='popup'>
                <div className='popup__close'>
                    <Close onClosePopup={this.props.closePopup} size='l'/>
                </div>
                <label className='popup__label' htmlFor="name">Введите название</label>
                <input className='popup__input' id='name' type="text"/>
                <div className='popup__btn-group'>
                    <Button text='Окей' />
                    <Button onClick={this.props.closePopup} text='Отмена' />
                </div>
            </form>,
            this.modal

        );
    }
}
