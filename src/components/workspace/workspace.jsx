import React, { Component } from 'react';
import Folder from '../folder';

import './workspace.css';

export default class Workspace extends Component {
    render() {
        return(
            <main className='workspace'>
                <section className='workspace__folder-section workspace__section'>
                    <Folder />
                </section>
                <section className='workspace__file-section workspace__section'>

                </section>
            </main>
        );
    }
}
