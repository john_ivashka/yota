import React, { Component } from 'react';

import Icon from '../icon/icon';

import './toolbar.css';

export default class Toolbar extends Component {
    render() {
        return (
            <div className='toolbar'>
                <button onClick={this.props.openPopup} className='toolbar__item'>
                    <Icon nameIcon='create_new_folder' />
                </button>
                <button onClick={this.props.openPopup} className='toolbar__item'>
                    <Icon nameIcon='content_paste' />
                </button>
            </div>
        );
    }
}
