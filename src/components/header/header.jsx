import React, { Component } from 'react';
import Logo from '../logo/logo.jsx';

import './header.css';

export default class Header extends Component {
    render() {
        return (
            <header className='header'>
                <h1 className='header__description'>Шаблоны для ответов</h1>
                <Logo />
            </header>
        );
    }
}
