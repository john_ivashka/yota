import React, { Component } from 'react';

import Header from './components/header';
import Toolbar from './components/toolbar';
import Workspace from './components/workspace';
import Popup from './components/popup';

import './vars.css';

class App extends Component {
  state = {
    isOpenPopup: false
  };

  onPopup = () => {
    this.setState(state => ({ isOpenPopup: true }));
  }

  offPopup = () => {
    this.setState(state => ({ isOpenPopup: false }));
  }

  render() {
    return (
      <div className="App">
        <Header />
        <Toolbar openPopup={this.onPopup}/>
        <Workspace />
        {this.state.isOpenPopup &&
          <Popup closePopup={this.offPopup} />
        }
      </div>
    );
  }
}

export default App;
